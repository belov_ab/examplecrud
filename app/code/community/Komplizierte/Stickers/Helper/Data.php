<?php
/**
 * Created by PhpStorm.
 * User: test
 * Date: 7/22/14
 * Time: 8:07 PM
 */ 
class Komplizierte_Stickers_Helper_Data extends Mage_Core_Helper_Abstract {
    CONST UPLOAD_FOLDER = 'stickers';
    public function getMediaUploadPath()
    {
        $media = Mage::getBaseDir('media');
        return $media.DS.self::UPLOAD_FOLDER.'/';
    }
    public function getMediaWebPath()
    {

        return '/media/'.self::UPLOAD_FOLDER.DS;
    }
}
<?php

class Komplizierte_Stickers_IndexController extends Mage_Core_Controller_Front_Action
{

    public function viewAction()
    {
        $this->loadLayout();

        $id = $this->getRequest()->getParam('show_block');
        $block=$this->getLayout()->createBlock('stickers/entity_content', 'entity', array('id'=>$id));
        $this->_addContent($block);

        $head = $this->getLayout()->getBlock('head');
        $head->setTitle($block->getSticker()->getName());
        $head->setKeywords($block->getSticker()->getMetaKeywords());
        $head->setDescription($block->getSticker()->getMetaDescription());
       
        $this->renderLayout();
    }

    protected function _addContent(Mage_Core_Block_Abstract $block)
    {
        $this->getLayout()->getBlock('content')->append($block);
        return $this;
    }
}
<?php

class Komplizierte_Stickers_Adminhtml_EntityController extends Mage_Adminhtml_Controller_Action
{
    protected function _initAction()
    {
        $this->_usedModuleName = 'entity';

        $this->loadLayout()
            ->_setActiveMenu('kit/kit')
            ->_title($this->__('Stickers control'))
            ->_addBreadcrumb($this->__('Stickers control'), $this->__('Stickers control'));

        return $this;
    }

    public function indexAction()
    {
        $this->_title($this->__('Stickers control'))->_title($this->__('Stickers control'));

        if ($this->getRequest()->getQuery('ajax')) {
            $this->_forward('grid');
            return;
        }
        $this->loadLayout();

        /**
         * Set active menu item
         */
        $this->_setActiveMenu('stickers/entity');

        /**
         * Append customers block to content
         */
        $this->_addContent(
            $this->getLayout()->createBlock('stickers/adminhtml_entity_entity', 'entity')
        );

        /**
         * Add breadcrumb item
         */
        $this->_addBreadcrumb($this->__('Stickers control'), $this->__('Stickers control'));
        $this->_addBreadcrumb($this->__('Stickers control'), $this->__('Stickers control'));

        $this->renderLayout();
    }

    public function gridAction()
    {
        $this->loadLayout();
        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('stickers/adminhtml_entity_entity_grid')->toHtml()
        );
    }

    public function newAction()
    {
        $this->_forward('view');
    }

    public function viewAction()
    {
        $id = $this->getRequest()->getParam('entity');
        $entity = Mage::getModel('komplizierte_stickers/entity')->load($id);
        if($entity->getId() || $id == 0){
            if($id){
                $this->_title($this->__('Sticker '.$id ));
            }else{
                $this->_title($this->__('New sticker'));
            }
            Mage::register('current_entity',$entity);
            $this->loadLayout();
            $this->_addContent(
                $this->getLayout()->createBlock('stickers/adminhtml_entity_edit', 'entity_edit')
            );
            $this->renderLayout();
        }else{
            $url = Mage::getUrl('*/*/index');
            $this->getResponse()->setRedirect($url);
        }
    }

    public function saveAction()
    {
        if ($data = $this->getRequest()->getPost()) {
            $model = Mage::getModel('komplizierte_stickers/entity');
            $model->setData($data)->setId($this->getRequest()->getParam('id'));
            try {
                if (isset($_FILES['img']['name']) and (file_exists($_FILES['img']['tmp_name']))) {

                    $uploader = new Varien_File_Uploader('img');
                    $uploader->setAllowedExtensions(array('jpg', 'jpeg', 'gif', 'png')); // or pdf or anything


                    $uploader->setAllowRenameFiles(false);

                    // setAllowRenameFiles(true) -> move your file in a folder the magento way
                    // setAllowRenameFiles(true) -> move your file directly in the $path folder
                    $uploader->setFilesDispersion(false);

                    $path = Mage::helper('komplizierte_stickers')->getMediaUploadPath();
                    $filename=md5($_FILES['img']['name']).'.'.$uploader->getFileExtension();
                    $uploader->save($path, $filename);
                    $model->setImg($filename);
                }
                Mage::getSingleton('adminhtml/session')->addSuccess($this->__('Sticker was successfully saved'));
                Mage::getSingleton('adminhtml/session')->setFormData(false);
                $model->save();
                $entity_id = $model->getId();

                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('id' => $entity_id));
                    return;
                }
                $this->_redirect('*/*/');
                return;

            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setFormData($data);
                $this->_redirect('*/*/edit', array('entity' => $this->getRequest()->getParam('entity')));
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('manager')->__('Error when saved'));
        $this->_redirect('*/*/');
    }



    public function deleteAction()
    {
        $id = $this->getRequest()->getParam('entity');
        $block = Mage::getModel('komplizierte_stickers/entity')->load($id);
        if($block->getId()){
            $block->delete();
            Mage::getSingleton('adminhtml/session')->addSuccess($this->__('Sticker was successful deleted'));
        }
        $this->_redirect('*/*/');
    }




}
<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Asus
 * Date: 05.11.13
 * Time: 19:24
 * To change this template use File | Settings | File Templates.
 */
class  Komplizierte_Stickers_Block_Adminhtml_Form_Element extends Varien_Data_Form_Element_Abstract {
    public function __construct($data) {
        parent::__construct($data);
        $this->setType('file');
    }

    public function getElementHtml() {
        $html = '';
        if ($this->getValue()) {
            $url = $this->_getUrl();
            if( !preg_match("/^http\:\/\/|https\:\/\//", $url) ) {
                $url = Mage::helper('komplizierte_stickers')->getMediaWebPath() . $url;
            }
            $html = '<a onclick="imagePreview(\''.$this->getHtmlId().'_image\'); return false;" href="'.$url.'"><img id="'.$this->getHtmlId().'_image" style="border: 1px solid #d6d6d6;" title="'.$this->getValue().'" src="'.$url.'" alt="'.$this->getValue().'" width="100"></a> ';
        }
        $this->setClass('input-file');
        $html.= parent::getElementHtml();
        return $html;
    }

    protected function _getUrl() {
        return $this->getValue();
    }

    public function getName() {
        return  $this->getData('name');
    }
}
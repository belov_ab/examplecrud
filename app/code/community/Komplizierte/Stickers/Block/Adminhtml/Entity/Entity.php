<?php

class Komplizierte_Stickers_Block_Adminhtml_Entity_Entity extends  Mage_Adminhtml_Block_Widget_Grid_Container
{

    public function __construct()
    {
        $this->_controller = 'adminhtml_entity_entity'; //Путь к блоку
        $this->_blockGroup = 'stickers'; //имя группы блоков config.xml
        $this->_headerText = $this->__('Stickers');
        parent::__construct();
    }
}
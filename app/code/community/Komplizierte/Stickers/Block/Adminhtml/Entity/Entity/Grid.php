<?php

class Komplizierte_Stickers_Block_Adminhtml_Entity_Entity_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

    public function __construct()
    {
        parent::__construct();
        $this->setId('entity_grid');
        $this->setUseAjax(true);
        $this->setDefaultSort('id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
    }
    protected function _prepareCollection()
    {
        $collection = Mage::getModel('komplizierte_stickers/entity')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {

        $this->addColumn('id', array(
            'header'=> $this->__('ID'),
            'width' => '20px',
            'type'  => 'number',
            'index' => 'id'
        ));

        $this->addColumn('name', array(
            'header'=> $this->__('Name'),
            'type'  => 'text',
            'index' => 'name',
        ));

        $this->addColumn('banner_group', array(
            'header'=> $this->__('Group'),
            'type'  => 'text',
            'width' => '20px',
            'index' => 'banner_group',
        ));
        $this->addColumn('position', array(
            'header'=> $this->__('position'),
            'type'  => 'text',
            'width' => '20px',
            'index' => 'position',
        ));
    }

    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/view', array('entity' => $row->getId()));
    }

    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current'=>true));
    }
}
<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Asus
 * Date: 23.10.13
 * Time: 21:58
 * To change this template use File | Settings | File Templates.
 */
class Komplizierte_Stickers_Block_Adminhtml_Entity_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        $this->_objectId = 'entity';
        $this->_blockGroup = 'stickers';
        $this->_controller = 'adminhtml_entity';

        parent::__construct();

        $this->_updateButton('save', 'label', $this->__('Save'));
        $this->_updateButton('delete', 'label', $this->__('Delete'));


    }


    public function getHeaderText()
    {
        return $this->__('Sticker');
    }

}
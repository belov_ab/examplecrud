<?php

class Komplizierte_Stickers_Block_Adminhtml_Entity_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareLayout()
    {
        $return = parent::_prepareLayout();
        if (Mage::getSingleton('cms/wysiwyg_config')->isEnabled()) {
            $this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
        }
        return $return;
    }

    protected function _prepareForm()
    {
        $form = new Varien_Data_Form(array(
            'id'        => 'edit_form',
            'action'    => $this->getData('action'),
            'method'    => 'post',
            'enctype'   => 'multipart/form-data'
        ));

        $entity = Mage::registry('current_entity');



        $form->setUseContainer(true);
        $this->setForm($form);

        $fieldset = $form->addFieldset('form_form',array('legend'=>$this->__('sticker data')));
        $fieldset->addType('thumbnail','Komplizierte_Stickers_Block_Adminhtml_Form_Element');

        $fieldset->addField('name', 'text', array(
            'label'     => $this->__('Name'),
            'class'     => 'required-entry',
            'required'  => true,
            'name'      => 'name',
            'style' => 'width:60em;'
        ));

        $fieldset->addField('img', 'thumbnail', array(
            'label'     => $this->__('Image'),
            'class'     => 'required-entry',
            'required'  => true,
            'name'      => 'img',
            'style' => 'width:60em;'
        ));
        $fieldset->addField('static_text', 'text', array(
            'label'     => $this->__('Static text'),
            'name'      => 'static_text',
            'style' => 'width:60em;'
        ));
        $fieldset->addField('position', 'text', array(
            'label'     => $this->__('Position'),
            'name'      => 'position',
            'style' => 'width:60em;'
        ));
        $fieldset->addField('img_alt', 'text', array(
            'label'     => $this->__('Img alt text'),
            'name'      => 'img_alt',
            'style' => 'width:60em;'
        ));
        $fieldset->addField('img_title', 'text', array(
            'label'     => $this->__('Img title text'),
            'name'      => 'img_title',
            'style' => 'width:60em;'
        ));

        $wysiwygConfig = Mage::getSingleton('cms/wysiwyg_config')->getConfig(array('add_variables' => false,
            'add_widgets' => false,
            'add_images' => true,
            'files_browser_window_url' => Mage::getSingleton('adminhtml/url')->getUrl('adminhtml/cms_wysiwyg_images/index'),
            'files_browser_window_width' => (int) Mage::getConfig()->getNode('adminhtml/cms/browser/window_width'),
            'files_browser_window_height'=> (int) Mage::getConfig()->getNode('adminhtml/cms/browser/window_height')
        ));

        $fieldset->addField('content', 'editor', array(
            'label'     => $this->__('Content'),
            'style' => 'height:30em; width:60em;',
            'config'    => $wysiwygConfig,
            'name'      => 'content',
        ));

        $fieldset->addField('banner_group', 'text', array(
        'label'     => $this->__('Group'),
        'class'     => 'required-entry',
        'required'  => true,
        'name'      => 'banner_group',
        'style' => 'width:60em;'
    ));


        $fieldset->addField('meta_keywords', 'textarea', array(
            'label'     => $this->__('Meta Keywords'),
            'style' => 'height:5em; width:60em;',
            'name'      => 'meta_keywords',
        ));

        $fieldset->addField('meta_description', 'textarea', array(
            'label'     => $this->__('Meta Description'),
            'style' => 'height:5em; width:60em;',
            'name'      => 'meta_description',
        ));

        if ($entity->getId()) {
            $form->addField('id', 'hidden', array(
                'name' => 'id',
            ));
            $form->setValues($entity->getData());
        }

        return parent::_prepareForm();
    }
}

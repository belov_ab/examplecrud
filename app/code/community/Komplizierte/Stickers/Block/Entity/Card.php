<?php

class Komplizierte_Stickers_Block_Entity_Card extends Mage_Core_Block_Template
{

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Render all stickers if its needed
     * @return string
     */
    public function _toHtml(){
        if($this->getId()=='all')
        {
            $html='';
            foreach($this->getStickers() as $sticker){
                $html.=$this->getStickerHtml($sticker->getId());
            }
            return $html;
        }
        return parent::_toHtml();
    }

    /**
     * get current sticker object
     * @return Mage_Core_Model_Abstract
     */
    public function getSticker(){
        return Mage::getModel('komplizierte_stickers/entity')->load($this->getId());
    }

    /**
     * get image for web
     * @return string
     */
    public function getImage(){
        return Mage::helper('komplizierte_stickers')->getMediaWebPath().$this->getSticker()->getImg();
    }

    /**
     * get all stickers
     * @return mixed
     */
    public function getStickers()
    {
        return Mage::getModel('komplizierte_stickers/entity')
            ->getCollection()
            ->addFieldToFilter('main_table.banner_group', $this->getGroup())->getItems();
    }

    /**
     * return block html code by element id
     * @param $id
     * @return mixed
     */
    public function getStickerHtml($id){
        return $this
                ->getLayout()
                ->createBlock('stickers/entity_card', 'entity_card', array('id'=>$id, 'group'=>$this->getGroup()))
                ->setTemplate($this->getTemplate())
                ->_toHtml();
    }

    /**
     * get url for ticket content page
     * @return string|void
     */
    public function getUrl(){
       return Mage::getUrl('k50/index/view/show_block', array('show_block' => $this->getSticker()->getid()));
    }
}
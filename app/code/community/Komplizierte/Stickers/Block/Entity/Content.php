<?php

class Komplizierte_Stickers_Block_Entity_Content extends Mage_Core_Block_Template
{

    public function __construct()
    {
        $this->setTemplate('stickers/sticker_content.phtml');
        parent::__construct();
    }

    public function getSticker()
    {
        return Mage::getModel('komplizierte_stickers/entity')->load($this->getId());
    }

    public function getParentLink()
    {
        return Mage::getUrl('k50');
    }

}